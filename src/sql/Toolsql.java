package sql;

import java.sql.Connection;//特定のデータベースとの接続(セッション)を表現します。
import java.sql.DriverManager;//一連のJDBCドライバを管理するための基本的なサービスです。
//注: JDBC 2.0 APIで新しく追加されたDataSourceインタフェースを使用してデータ・ソースに接続することも可能です。
import java.sql.PreparedStatement;//プリコンパイルされたSQL文を表すオブジェクトです。
import java.sql.ResultSet;//データベースの結果セットを表すデータの表で、通常、データベースに照会する文を実行することによって生成されます。
import java.sql.SQLException;//データベース・アクセス・エラーまたはその他のエラーに関する情報を提供する例外です。
import java.util.ArrayList;

import model.Tool;

public class Toolsql {
	//ArrayListを使いTool型のitemListインスタンスの作成
	ArrayList<Tool> itemList = new ArrayList<Tool>();
		//ArrayList<Tool>型のgetItemメッソドを作成
		public ArrayList<Tool> getItem(){
		//Connection型のconn変数にnullを代入
		Connection conn = null;

		final String DRIVER_NAME = "com.mysql.jdbc.Driver";//MySQLドライバ
		final String DB_URL = "jdbc:mysql://localhost:3306/";//DBサーバー名
		final String DB_NAME = "Shinya";//データベース名
		final String DB_ENCODE = "?useUnicode=true&characterEncoding=utf8";//文字化け防
		final String DB_USER = "root";//ユーザーID
		final String DB_PASS = "root";//パスワード
		try{
			//JDBCドライバの読み込み
			Class.forName(DRIVER_NAME);
			//データベース、ユーザーID、パスワード読み込みconnに代入
			conn = DriverManager.getConnection
					(DB_URL + DB_NAME+DB_ENCODE , DB_USER, DB_PASS);
			//select文を準備
			String sql = "SELECT * FROM Tool";//データを取ってくる・フィールド名・FROM・テーブル名
			//
			PreparedStatement pStmt = conn.prepareStatement(sql);
			//SELECT文を実効し、結果表を取得
			ResultSet rs = pStmt.executeQuery();
			//結果表に格納されたレコードの内容を表示

			//レコード数分処理を繰り返し
			while(rs.next()){
				//フィールドの値を変数に代入
				int id = rs.getInt("id");
				int number = rs.getInt("number");
				String name = rs.getString("name");
				String brand = rs.getString("brand");
				String size = rs.getString("size");
				String place = rs.getString("place");
				//Toolインスタンスの作成引数にid, number, name, brand, size, placeを入れる
				Tool t = new Tool(id, number, name, brand, size, place);
				//itemListの配列にToolのデータを格納
				itemList.add(t);
			}

		} catch(SQLException | ClassNotFoundException e){
			e.printStackTrace();
		}finally{
			//データベース切断
			if(conn != null){
				try{
					conn.close();
				}catch(SQLException e){
					e.printStackTrace();

				}
			}
		}
		return itemList;
	}


}
