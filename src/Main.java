import java.util.ArrayList;

import model.Tool;
import sql.Toolsql;

public class Main {

	public static void main(String[] args) {
		//Ｔｏｏｌｓｑｌ型のインスタンスの作成
		Toolsql ts = new Toolsql();

		ArrayList<Tool>tools=ts.getItem();

		for(Tool t : tools){
			int i =t.getId();
			String n = t.getName();
			int nu = t.getNumber();
			String b = t.getBrand();
			String s = t.getSize();
			String p =t.getPlace();
			System.out.println(i+": "+n+" ・"+b+" ・"+nu+" ・"+s+" ・"+p);
		}
	}

}
