package model;

public class Tool {
	private int id ;//id番号
	private int number;//個数
	private String name;//名前
	private String brand;//メーカー
	private String size;//大きさ
	private String place;//収納場所
	/**
	 * @param id
	 * @param number
	 * @param name
	 * @param brand
	 * @param size
	 * @param place
	 */
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getNumber() {
		return number;
	}
	public void setNumber(int number) {
		this.number = number;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getBrand() {
		return brand;
	}
	public void setBrand(String brand) {
		this.brand = brand;
	}
	public String getSize() {
		return size;
	}
	public void setSize(String size) {
		this.size = size;
	}

	public String getPlace() {
		return place;
	}
	public void setPlace(String place) {
		this.place = place;
	}
	public Tool(int id, int number, String name, String brand, String size ,String place) {
		super();
		this.id = id;
		this.number = number;
		this.name = name;
		this.brand = brand;
		this.size = size;
		this.place = place;

	}
}
