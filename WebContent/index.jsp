<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import= "model.Tool" import="sql.Toolsql" import = "java.util.ArrayList"%>
<%
Toolsql ts = new Toolsql();
ArrayList<Tool>tools=ts.getItem();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>Insert title here</title>
		<link rel="stylesheet" href="css/index.css">
	</head>
	<body>
		<ul class="Tools">
			<%for(Tool t : tools){ %>
			<li> ID: <%=t.getId() %></li>
			<li> Tool名: <%=t.getName() %></li>
			<li> メーカー: <%= t.getBrand() %></li>
			<li> サイズ: <%= t.getSize() %></li>
			<li> 保管場所: <%=t.getPlace()%></li>
			<li> 個数: <%=t.getNumber()%></li><br>
			<%} %>
		</ul>
	</body>
</html>